function layThongTinTuForm() {
  const taiKhoan = document.getElementById("tknv").value;
  const name = document.getElementById("name").value;
  const email = document.getElementById("email").value;
  const password = document.getElementById("password").value;
  const ngayLam = document.getElementById("datepicker").value;
  const luongCB = document.getElementById("luongCB").value * 1;
  const chucVu = document.getElementById("chucvu").value;
  const gioLam = document.getElementById("gioLam").value * 1;
  //tạo Nhân Viên mới từ object class đã được định nghĩa bên nvModel.js
  var nv = new NhanVien(
    taiKhoan,
    name,
    email,
    password,
    ngayLam,
    luongCB,
    chucVu,
    gioLam
  );
  return nv;
}
function renderDSNV(nvArr) {
  var contentHTML = "";
  for (var index = 0; index < nvArr.length; index++) {
    var currentNv = nvArr[index];
    var newCurrentNV = new NhanVien(
      currentNv.taiKhoanNV,
      currentNv.tenNV,
      currentNv.email,
      currentNv.matKhau,
      currentNv.ngayLamViec,
      currentNv.luongCoBan,
      currentNv.chucVu,
      currentNv.gioLam
    );
    var contentTr = `
    <tr>
    <td>${newCurrentNV.taiKhoanNV}</td>
    <td>${newCurrentNV.tenNV}</td>
    <td>${newCurrentNV.email}</td>
    <td>${newCurrentNV.ngayLamViec}</td>
    <td>${newCurrentNV.chucVu}</td>
    <td>${newCurrentNV.tinhTongLuong()}</td>
    <td>${newCurrentNV.xepLoai()}</td>
    <td><button onclick="xoaNv('${
      newCurrentNV.taiKhoanNV
    }')" class="btn btn-danger">Xoá</button>
    <button onclick="suaNv('${
      newCurrentNV.taiKhoanNV
    }')" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Sửa</button></td>
    </tr>
    `;
    contentHTML += contentTr;
  }
  //   Hiển thị ra màn hình bằng cách DOM tới thẻ tbody
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function timKiemViTri(taiKhoan, nvArr) {
  for (var index = 0; index < nvArr.length; index++) {
    var item = nvArr[index];
    if (item.taiKhoanNV == taiKhoan) {
      return index;
    }
  }
  return -1;
}
function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoanNV;
  document.getElementById("name").value = nv.tenNV;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLamViec;
  document.getElementById("luongCB").value = nv.luongCoBan;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}
function resetForm() {
  document.getElementById("formQLNV").reset();
}
function resetButtonFormThemNv() {
  document.getElementById("tknv").disabled = false;
  document.getElementById("formQLNV").reset();
  document.getElementById("btnCapNhat").style.display = "none";
  document.getElementById("btnThemNV").style.display = "block";
}
function resetButtonFormSuaNv() {
  document.getElementById("btnThemNV").style.display = "none";
  document.getElementById("btnCapNhat").style.display = "block";
}
function showMessageErr(idThongBao, message) {
  document.getElementById(idThongBao).innerHTML = message;
  document.getElementById(idThongBao).style.display = "block";
}
function formValidation(nv) {
  var isValid = true;
  //validate Tài Khoản Nhân Viên
  isValid =
    (kiemTraRong(nv.taiKhoanNV, "tbTKNV", "Tài khoản không được để trống") &&
      kiemTraCharLength(
        nv.taiKhoanNV,
        "tbTKNV",
        "Tài khoản chỉ được nhập tối đa 6 ký tự"
      ) &&
      kiemTraTrung(nv, dsnv)) &
    // validate Tên Nhân Viên
    (kiemTraRong(nv.tenNV, "tbTen", "Tên nhân viên không được để trống") &&
      kiemTraName(nv.tenNV, "tbTen", "Tên nhân viên phải là chữ")) &
    //Validate email
    (kiemTraRong(nv.email, "tbEmail", "Email không được để trống") &&
      kiemTraEmail(nv.email, "tbEmail", "Email phải đúng định dạng")) &
    //Validate password
    (kiemTraRong(nv.matKhau, "tbMatKhau", "Mật khẩu không được để trống") &&
      kiemTraPassword(
        nv.matKhau,
        "tbMatKhau",
        "Mật khẩu phải chứa ít nhất: 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt, độ dài từ 6-10 ký tự"
      )) &
    //validate Ngày Làm Việc
    (kiemTraRong(
      nv.ngayLamViec,
      "tbNgay",
      "Ngày làm việc không được để trống"
    ) &&
      kiemTraNgayLamViec(
        nv.ngayLamViec,
        "tbNgay",
        "Ngày làm việc phải đúng định dạng"
      )) &
    //validate Lương Cơ Bản
    (kiemTraRong(
      nv.luongCoBan,
      "tbLuongCB",
      "Lương cơ bản không được để trống"
    ) &&
      kiemTraLuongCoBan(
        nv.luongCoBan,
        "tbLuongCB",
        "Lương cơ bản phải từ 1.000.000đ đến 20.000.000đ"
      )) &
    //validate Lương Cơ Bản
    kiemTraChucVu(nv.chucVu, "tbChucVu", "Chức vụ không được để trống") &
    //validate Giờ Làm
    (kiemTraRong(nv.gioLam, "tbGiolam", "Giờ làm không được để trống") &&
      kiemTraGioLam(
        nv.gioLam,
        "tbGiolam",
        "Giờ làm trong tháng phải từ 80 - 200 giờ"
      ));
  if (isValid) {
    dsnv.push(nv);
    luuLocalStorage();
    renderDSNV(dsnv);
    resetForm();
  } else {
  }
}
function capNhatValidation(nv) {
  var isValid = true;
  isValid =
    // validate Tên Nhân Viên
    (kiemTraRong(nv.tenNV, "tbTen", "Tên nhân viên không được để trống") &&
      kiemTraName(nv.tenNV, "tbTen", "Tên nhân viên phải là chữ")) &
    //Validate email
    (kiemTraRong(nv.email, "tbEmail", "Email không được để trống") &&
      kiemTraEmail(nv.email, "tbEmail", "Email phải đúng định dạng")) &
    //Validate password
    (kiemTraRong(nv.matKhau, "tbMatKhau", "Mật khẩu không được để trống") &&
      kiemTraPassword(
        nv.matKhau,
        "tbMatKhau",
        "Mật khẩu phải chứa ít nhất: 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt, độ dài từ 6-10 ký tự"
      )) &
    //validate Ngày Làm Việc
    (kiemTraRong(
      nv.ngayLamViec,
      "tbNgay",
      "Ngày làm việc không được để trống"
    ) &&
      kiemTraNgayLamViec(
        nv.ngayLamViec,
        "tbNgay",
        "Ngày làm việc phải đúng định dạng"
      )) &
    //validate Lương Cơ Bản
    (kiemTraRong(
      nv.luongCoBan,
      "tbLuongCB",
      "Lương cơ bản không được để trống"
    ) &&
      kiemTraLuongCoBan(
        nv.luongCoBan,
        "tbLuongCB",
        "Lương cơ bản phải từ 1.000.000đ đến 20.000.000đ"
      )) &
    //validate Lương Cơ Bản
    kiemTraChucVu(nv.chucVu, "tbChucVu", "Chức vụ không được để trống") &
    //validate Giờ Làm
    (kiemTraRong(nv.gioLam, "tbGiolam", "Giờ làm không được để trống") &&
      kiemTraGioLam(
        nv.gioLam,
        "tbGiolam",
        "Giờ làm trong tháng phải từ 80 - 200 giờ"
      ));
  if (isValid) {
    document.getElementById("tknv").disabled = false;
    var viTri = timKiemViTri(nv.taiKhoanNV, dsnv);
    if (viTri == -1) return;
    dsnv[viTri] = nv;
    renderDSNV(dsnv);
    luuLocalStorage();
    resetForm();
  } else {
  }
}
