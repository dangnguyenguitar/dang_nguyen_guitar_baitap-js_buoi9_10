function kiemTraTrung(nv, array) {
  var index = timKiemViTri(nv.taiKhoanNV, array);
  if (index !== -1) {
    //nếu index này đã có sẵn trong array rồi thì return false
    showMessageErr("tbTKNV", `Tài khoản này đã tồn tại!`);
    return false;
  } else {
    showMessageErr("tbTKNV", "");
    return true;
  }
}

function kiemTraRong(userInput, taiKhoanErr, message) {
  if (userInput.length == 0 || userInput == 0) {
    showMessageErr(taiKhoanErr, message);
    return false;
  } else {
    showMessageErr(taiKhoanErr, "");
    return true;
  }
}

function kiemTraCharLength(userInput, taiKhoanErr, message) {
  if (userInput.length > 6) {
    showMessageErr(taiKhoanErr, message);
    return false;
  } else {
    showMessageErr(taiKhoanErr, "");
    return true;
  }
}
function kiemTraName(userInput, taiKhoanErr, message) {
  var reg = /^[^\d]+$/;
  var isName = reg.test(userInput);
  if (isName) {
    showMessageErr(taiKhoanErr, "");
    return true;
  } else {
    showMessageErr(taiKhoanErr, message);
    return false;
  }
}
function kiemTraEmail(userInput, taiKhoanErr, message) {
  var reg = /(.+)@(.+){2,}\.(.+){2,}/;
  var isEmail = reg.test(userInput);
  if (isEmail) {
    showMessageErr(taiKhoanErr, "");
    return true;
  } else {
    showMessageErr(taiKhoanErr, message);
    return false;
  }
}

function kiemTraPassword(userInput, taiKhoanErr, message) {
  var pwReg = new RegExp("^(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{6,10})");
  var isPassword = pwReg.test(userInput);
  if (isPassword) {
    showMessageErr(taiKhoanErr, "");
    return true;
  } else {
    showMessageErr(taiKhoanErr, message);
    false;
  }
}
function kiemTraNgayLamViec(userInput, taiKhoanErr, message) {
  var dateReg = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
  var isDate = dateReg.test(userInput);
  if (isDate) {
    showMessageErr(taiKhoanErr, "");
    return true;
  } else {
    showMessageErr(taiKhoanErr, message);
    false;
  }
}
function kiemTraLuongCoBan(userInput, taiKhoanErr, message) {
  if (userInput >= 1000000 && userInput <= 20000000) {
    showMessageErr(taiKhoanErr, "");
    return true;
  } else {
    showMessageErr(taiKhoanErr, message);
    false;
  }
}
function kiemTraChucVu(userInput, taiKhoanErr, message) {
  if (userInput == "Chọn chức vụ") {
    showMessageErr(taiKhoanErr, message);
    return false;
  } else {
    showMessageErr(taiKhoanErr, "");
    return true;
  }
}
function kiemTraGioLam(userInput, taiKhoanErr, message) {
  if (userInput >= 80 && userInput <= 200) {
    showMessageErr(taiKhoanErr, "");
    return true;
  } else {
    showMessageErr(taiKhoanErr, message);
    return false;
  }
}
