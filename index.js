var dsnv = [];
var dataJson = localStorage.getItem("DSNV");
if (dataJson !== null) {
  var nvArr = JSON.parse(dataJson);
  for (var index = 0; index < nvArr.length; index++) {
    var item = nvArr[index];
    var nv = new NhanVien(
      item.taiKhoanNV,
      item.tenNV,
      item.email,
      item.matKhau,
      item.ngayLamViec,
      item.luongCoBan,
      item.chucVu,
      item.gioLam
    );
    dsnv.push(nv);
  }
  renderDSNV(nvArr);
}
function luuLocalStorage() {
  let jsonDsnv = JSON.stringify(dsnv);
  localStorage.setItem("DSNV", jsonDsnv);
}
function themNv() {
  var nv = layThongTinTuForm();
  formValidation(nv);
}
function xoaNv(taiKhoan) {
  var viTri = timKiemViTri(taiKhoan, dsnv);
  if (viTri !== -1) {
    dsnv.splice(viTri, 1);
    luuLocalStorage();
    renderDSNV(dsnv);
  }
}
function suaNv(taiKhoan) {
  var viTri = timKiemViTri(taiKhoan, dsnv);
  if (viTri == -1) return;
  var data = dsnv[viTri];
  showThongTinLenForm(data);
  resetButtonFormSuaNv();
  document.getElementById("tknv").disabled = true;
}
function capNhatNv() {
  document.getElementById("tknv").disabled = true;
  var data = layThongTinTuForm();
  capNhatValidation(data);
}
function timNvTheoXepLoai() {}
