function NhanVien(
  taiKhoanNV,
  tenNV,
  email,
  matKhau,
  ngayLamViec,
  luongCoBan,
  chucVu,
  giolam
) {
  this.taiKhoanNV = taiKhoanNV;
  this.tenNV = tenNV;
  this.email = email;
  this.matKhau = matKhau;
  this.ngayLamViec = ngayLamViec;
  this.luongCoBan = luongCoBan;
  this.chucVu = chucVu;
  this.gioLam = giolam;
  this.tinhTongLuong = function () {
    if (this.chucVu == "Sếp") {
      return this.luongCoBan * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      return this.luongCoBan * 2;
    } else {
      return this.luongCoBan * 1;
    }
  };
  this.xepLoai = function () {
    if (this.gioLam >= 192) {
      return "Nhân viên xuất sắc";
    } else if (this.gioLam >= 176) {
      return "Nhân viên giỏi";
    } else if (this.gioLam >= 160) {
      return "Nhân viên khá";
    } else {
      return "Nhân viên trung bình";
    }
  };
}
